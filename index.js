// class Human {
//   // add static property
//   static isLivingOnEarth = true;

//   // add constructor method
//   constructor(name, address) {
//     this.name = name;
//     this.address = address;
//   }

//   // Add instance method signature
//   introduce() {
//     console.log(`Hi, my name is ${this.name}`);
//   }
// }

// console.log(Human.isLivingOnEarth);

// // add prototype/instance method
// Human.prototype.greet = function (name) {
//   console.log(`Hi ${name}, I'm ${this.name}`);
// };

// // add static method
// Human.destroy = function (thing) {
//   console.log(`Human is destroying ${thing}`);
// };

// // Instatiation of Human class, we create a new object
// let fardan = new Human("Fardan", "Bantul");
// console.log(fardan);

// // checking instance of class
// console.log(fardan instanceof Human);
// console.log(fardan.introduce());
// console.log(fardan.greet("nozami"));

// class Human {
//   constructor(name, address) {
//     this.name = name;
//     this.address = address;
//   }

//   introduce() {
//     console.log(`hello My name is ${this.name}`);
//   }

//   work() {
//     console.log(`work`);
//   }
// }

// // create child class from Human
// class Programmer extends Human {
//   constructor(name, address, programmingLanguage) {
//     // call parent class constructor
//     super(name, address);
//     this.programmingLanguage = programmingLanguage;
//   }

// Override introduce method
//   introduce() {
//     // call parent class method
//     super.introduce();
//     console.log(`I can write ${this.programmingLanguage}`);
//   }

// Overloading introduce method
//   introduce(gender) {
//     super.introduce(); //call parent method
//     console.log(`I'm ${gender} and I can Write ${this.programmingLanguage}`);
//   }
// }

// let fardan = new Human("Fardan", "Bantul");
// fardan.introduce();

// let nozami = new Programmer("Nozami", "Surabaya", [
//   "Javascript",
//   "Python",
//   "PHP",
// ]);
// nozami.introduce("male");
// nozami.work();

// class User {
//   constructor(props) {
//     // props is object because it is better that way
//     let { email, password } = props; //destruct
//     this.email = email;
//     this.encryptedPassword = this.#encryp(password);
//     // we won't save the plain password
//   }

//   // private method
//   #encryp = (password) => {
//     return `encrypted-version-of-${password}`;
//   };

//   // Getter
//   #decrypt = () => {
//     return this.encryptedPassword.split(`encrypted-version-of-`)[1];
//   };

//   authenticate(password) {
//     return this.#decrypt() === password; //will return true or false
//   }
// }

// let bot = new User({
//   email: "bot@gmail.com",
//   password: "123",
// });

// console.log(bot.authenticate("123"));

// abstraction
// class Human {
//   constructor(props) {
//     if (this.constructor === Human) {
//       throw new Error("Cannot instantiate from Abstract Class");
//       // because it's abstract
//     }

//     let { name, address } = props;
//     this.name = name; //Every human has  name
//     this.address = address; //every human has address
//     this.proffesion = this.constructor.name; //every human has proffesion and let the child to define it
//   }

//   work() {
//     console.log(`working`);
//   }

//   introduce() {
//     console.log(`My Name is ${this.name}`);
//   }
// }

// class Police extends Human {
//   constructor(props) {
//     super(props);
//     this.rank = props.rank; // add new property rank
//   }

//   work() {
//     super.work();
//     console.log(`go to the police station`);
//   }
// }

// const Nozami = new Police({
//   name: "Nozami",
//   address: "Bantul",
//   rank: "kapolri",
// });

// console.log(Nozami.proffesion);
// console.log(Nozami.work());
// console.log(Nozami.introduce());

// try {
//   let Abstract = new Human({
//     name: "Abstract",
//     address: "jogja",
//   });
// } catch (err) {
//   console.log(err.message);
// }

// Polymorphism
class Human {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  introduce() {
    console.log(`Hello my name is ${this.name}`);
  }

  work() {
    console.log(`${this.constructor.name}, Working!`);
  }
}

// public server module/helper
const PublicServer = (Base) =>
  class extends Base {
    save() {
      console.log(`SFX : Thank you`);
    }
  };

// military module/helper
const Military = (Base) =>
  class extends Base {
    shoot() {
      console.log(`DOR!`);
    }
  };

class Doctor extends PublicServer(Human) {
  constructor(props) {
    super(props);
  }

  work() {
    super.work(); //from human class
    super.save(); //from PublicServer class
  }
}

class Police extends PublicServer(Military(Human)) {
  static workplace = "Police Station";

  constructor(props) {
    super(props);
    this.rank = props.rank;
  }

  work() {
    super.work(); //from Human Class
    super.shoot(); //from Miliraty Class
    super.save(); //from publicServer Class
  }
}
class Army extends PublicServer(Military(Human)) {
  static workplace = "Army Station";

  constructor(props) {
    super(props);
    this.rank = props.rank;
  }

  work() {
    super.work(); //from Human Class
    super.shoot(); //from Miliraty Class
    super.save(); //from publicServer Class
  }
}

class Writer extends Human {
  work() {
    super.work();
    console.log(`Write Books`);
  }
}

// instantiate Militaru based Class
const Fardan = new Police({
  name: "Fardan",
  address: "Bantul",
  rank: "general Commander",
});

const Nozami = new Army({
  name: "Nozami",
  address: "Surabaya",
  rank: "Mayor jendral",
});

// Instantiate Doctor
const Ajitama = new Doctor({
  name: "Ajitama",
  address: "Jogja",
});

// Instantiate writer
const Efena = new Writer({
  name: "Efena",
  address: "DIY",
});

Fardan.shoot();
Nozami.shoot();
Fardan.save();
Nozami.save();
Ajitama.save();
Fardan.work();
Nozami.work();
Ajitama.work();
Efena.work();
